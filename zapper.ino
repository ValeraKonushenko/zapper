#include <Servo.h>

class Door{
private:
  bool is_open;  
  bool wait_req;
public:
  struct Pins{
    const int  l_r = 2;
    const int  l_g = 3;
    const int  bt_r = 4;
    const int  bt_g = 5;
    const int  rng = 6;
    const int  serv = 0;
  };
  Servo servo;
  Pins pin;
  Door(){
    is_open = false;
    wait_req = false;
  }
  void Ring(){
    wait_req = true;
  }
  void SetReq(){
    servo.write(180);
    is_open = true;
    wait_req = false;
  }
  void Close(){
    servo.write(0);
    wait_req = false;
    is_open = false;
  }
  bool IsOpen(){
    return is_open;
  }
  bool IsReq(){
    return wait_req;
  }
};
Door door;

void setup() 
{
  
  door.servo.attach(door.pin.serv);
  door.servo.write(180);
  pinMode(door.pin.l_r, OUTPUT);
  pinMode(door.pin.l_g, OUTPUT);
  pinMode(door.pin.rng, OUTPUT);
  pinMode(door.pin.bt_r, INPUT);
  pinMode(door.pin.bt_g, INPUT);
  
  Serial.begin(9600);                          // Задаем скорость передачи данных
  
}
void loop() 
{    
             
  if(digitalRead(door.pin.bt_r) == HIGH){
    door.Ring();
    digitalWrite(6, HIGH);
    Serial.println("Ring");
  }
  else
    digitalWrite(6, LOW);
    
  if(digitalRead(door.pin.bt_g) == HIGH && door.IsReq()){
    door.SetReq();
    Serial.println("set req. Open door");
  }
  if(door.IsOpen()){
    if(digitalRead(door.pin.bt_r))
      door.Close();
    digitalWrite(door.pin.l_g, HIGH);
    digitalWrite(door.pin.l_r, LOW);
    Serial.println("Door is opened");
  }
  else{
    digitalWrite(door.pin.l_g, LOW);
    digitalWrite(door.pin.l_r, HIGH);
  }
  delay(1000);
  door.servo.write(0);
  delay(1000);
  door.servo.write(180);
}
